# Chapter 1. Getting Started

## Exercise 1.1

De acordo com a Seção [3.3 Compiling C++ Programs](https://gcc.gnu.org/onlinedocs/gcc-9.3.0/gcc/Invoking-G_002b_002b.html) do manual do GCC,
são adotadas as seguintes convenções:

* Arquivos-fonte: `.C` `.cc` `.cpp` `.CPP` `.c++` `.cp` `.cxx`
* Arquivos de cabeçalho: `.hh` `.hpp` `.H` `.tcc`
* Arquivos pré-processados: `.ii`

Além desses, arquivos com os sufixos `.c`, `.h` e `.i` são tratados como arquivos C++ pelo compilador g++.

---

[exercise1_01.cpp](exercise1_01.cpp)

```sh
$ g++ exercise1_01.cpp -o exercise1_01
$ ./exercise1_01
```


## Exercise 1.2

[exercise1_02.cpp](exercise1_02.cpp)

```sh
# No GNU/Linux
$ g++ exercise1_02.cpp -o exercise1_02
$ ./exercise1_02
$ echo $?
255
```

O inteiro `-1` retornado pelo programa `exercise1_02` possui 32 bits e sua
representação em binário é `11111111 11111111 11111111 11111111`, mas em
sistemas **UNIX** (ou UNIX-like, como o Linux), os códigos retornados pelos
programas são interpretados como números inteiros **sem sinal e de 8 bits**.
Então, dos 32 bits do valor retornado pelo programa, apenas os 8 bits menos 
significativos serão avaliados (~~11111111 11111111 11111111~~ 11111111). 
O número binário `11111111` **sem sinal** equivale ao decimal `255`. Por isso,
a saída do comando `echo $?` é `255`.


### Exercise 1.3

[exercise1_03.cpp](exercise1_03.cpp)


### Exercise 1.4

[exercise1_04.cpp](exercise1_04.cpp)


### Exercise 1.5

[exercise1_05.cpp](exercise1_05.cpp)


### Exercise 1.6

```cpp
// fragmento ilegal
std::cout << "The sum of " << v1;
          << " and " << v2;
          << " is " << v1 + v2 << std::endl;
```

No fragmento acima, a primeira ocorrência de `;` está interrompendo o encadeamento
de chamadas do operador de inserção `<<`, tornando o fragmento ilegal.

Possíveis formas de corrigir esse fragmento de código:

* Remover os `;` das linhas 1 e 2, formando assim um único encadeamento de chamadas do operador `<<`:

    ```cpp
    std::cout << "The sum of " << v1
              << " and " << v2
              << " is " << v1 + v2 << std::endl;
    ```

* Inserir `std::cout` nas linhas 2 e 3, formando 2 novos encadeamentos de chamadas do operador `<<`:

    ```cpp
    std::cout << "The sum of " << v1;
    std::cout << " and " << v2;
    std::cout << " is " << v1 + v2 << std::endl;
    ```


### Exercise 1.7

[exercise1_07.cpp](exercise1_07.cpp)

```sh
$ g++ exercise1_07.cpp -o exercise1_07 
exercise1_07.cpp:2:25: error: ‘cannot’ does not name a type
 * comment pairs /*   */ cannot nest.
                         ^~~~~~
```


### Exercise 1.8

```cpp
std::cout << "/*";                  // legal
std::cout << "*/";                  // legal
std::cout << /* "*/" */;            // ilegal
std::cout << /*  "*/" /* "/*"  */;  // ilegal
```

---

[exercise1_08.cpp](exercise1_08.cpp)

```sh
$ g++ exercise1_08.cpp -o exercise1_08
exercise1_08.cpp:7:24: warning: missing terminating " character
     std::cout << /* "*/" */;
                        ^
exercise1_08.cpp:7:24: error: missing terminating " character
     std::cout << /* "*/" */;
                        ^~~~~
exercise1_08.cpp: In function ‘int main()’:
exercise1_08.cpp:7:15: error: no match for ‘operator<<’ (operand types are ‘std::ostream {aka std::basic_ostream<char>}’ and ‘std::ostream {aka std::basic_ostream<char>}’)
     std::cout << /* "*/" */;
     ~~~~~~~~~~^~~~~~~~~~~~~~
     std::cout << /*  "*/" /* "/*"  */;
     ~~~
```


### Exercise 1.9

[exercise1_09.cpp](exercise1_09.cpp)


### Exercise 1.10

[exercise1_10.cpp](exercise1_10.cpp)


### Exercise 1.11

[exercise1_11.cpp](exercise1_11.cpp)


### Exercise 1.12

Esse laço `for` soma todos os números inteiros no intervalo [-100; 100]. O valor final de `sum` é zero.


### Exercise 1.13

[exercise1_13a.cpp](exercise1_13a.cpp)

[exercise1_13b.cpp](exercise1_13b.cpp)

[exercise1_13c.cpp](exercise1_13c.cpp)

### Exercise 1.14

O `for` agrupa em seu cabeçalho as operações de inicialização, teste e atualização de uma variável de controle do laço de repetição. Por isso, o `for` é normalmente utilizado para implementar laços cujo número de repetições é determinado por uma variável de controle (como um contador, por exemplo).

O `while` é uma estrutura de controle muito mais simples que o `for` e é normalmente utilizado para implementar laços cujo número de repetições é indeterminado.


### Exercise 1.15

[exercise1_15a.cpp](exercise1_15a.cpp)

[exercise1_15b.cpp](exercise1_15b.cpp)

[exercise1_15c.cpp](exercise1_15c.cpp)


### Exercise 1.16

[exercise1_16.cpp](exercise1_16.cpp)


### Exercise 1.17

Se todas as entradas forem iguais o programa irá apresentar apenas uma linha com o número de ocorrências daquela entrada.

Se não existirem duplicatas o programa irá mostrar que cada entrada ocorreu apenas uma vez.


### Exercise 1.18

[exercise1_18.cpp](exercise1_18.cpp)

```sh
$ g++ exercise1_18.cpp -o exercise1_18

$ ./exercise1_18
2 2 2 2 2 2           
2 occurs 6 times

$ ./exercise1_18
1 2 3 4 5 6           
1 occurs 1 times
2 occurs 1 times
3 occurs 1 times
4 occurs 1 times
5 occurs 1 times
6 occurs 1 times
```


### Exercise 1.19

[exercise1_19.cpp](exercise1_19.cpp)


### Exercise 1.20

[exercise1_20.cpp](exercise1_20.cpp)


### Exercise 1.21

[exercise1_21.cpp](exercise1_21.cpp)


### Exercise 1.22

[exercise1_22.cpp](exercise1_22.cpp)


### Exercise 1.23

[exercise1_23.cpp](exercise1_23.cpp)


### Exercise 1.24

```sh
$ ./exercise1_23 < data/book_sales
Transactions for 0-201-70353-X occurs 1 times.
Transactions for 0-201-82470-1 occurs 1 times.
Transactions for 0-201-88954-4 occurs 4 times.
Transactions for 0-399-82477-1 occurs 2 times.
Transactions for 0-201-78345-X occurs 2 times.
```


### Exercise 1.25

[exercise1_25.cpp](exercise1_25.cpp)

```sh
$ g++ exercise1_25.cpp -o exercise1_25
$ ./exercise1_25 < data/book_sales
0-201-70353-X 4 99.96 24.99
0-201-82470-1 4 181.56 45.39
0-201-88954-4 16 198 12.375
0-399-82477-1 5 226.95 45.39
0-201-78345-X 5 110 22
```

#include <iostream>

int main()
{
    std::cout << "Enter two integers (lower first): ";
    int lo, hi;
    std::cin >> lo >> hi;

    if (lo > hi) {
        // troca os valores de lo e hi
        int tmp = lo;
        lo = hi;
        hi = tmp;
    }

    // imprime um intervalo semiaberto à direita - [lo, hi)
    while (lo < hi) {
        std::cout << lo << " ";
        ++lo;
    }
    std::cout << std::endl;

    return 0;
}

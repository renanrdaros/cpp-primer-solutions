// syntax errors

#include <iostream>

// falta o parêntese de fechamento na lista de parâmetros da função main
int main ( {
    // no final da linha, onde há ':', deveria ser ';'
    std::cout << "Read each file." << std::endl:
    // strings literais devem ser envolvidas por aspas
    std::cout << Update master. << std::endl;
    // falta o operador de inserção '<<' entre a string literal e o std::endl
    std::cout << "Write new master." std::endl;
    // falta o ';'?
    return 0
}

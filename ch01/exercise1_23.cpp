#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item current;

    if (std::cin >> current) {
        Sales_item previous = current;
        int count = 1;

        while (std::cin >> current) {
            if (previous.isbn() == current.isbn()) {
                ++count;
            }
            else {
                std::cout << "Transactions for " << previous.isbn() << " occurs " << count << " times." << std::endl;   
                count = 1;
                previous = current;
            }
        }

        std::cout << "Transactions for " << previous.isbn() << " occurs " << count << " times." << std::endl;   
    }

    return 0;
}

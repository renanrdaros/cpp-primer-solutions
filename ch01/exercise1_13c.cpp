/*
 * Nota : Em programação de computadores, ranges são normalmente semiabertos à direita - [lo, hi).
 *        Por isso, esta solução imprime lo mas não hi.
 ***************************************************************************************************/

#include <iostream>

int main()
{
    std::cout << "Enter two numbers (lower first): ";
    int lo, hi;
    std::cin >> lo >> hi;

    for ( ; lo < hi; ++lo) {
        std::cout << lo << " ";
    }
    std::cout << std::endl;

    return 0;
}

#include <iostream>

int main()
{
    int i = 0, &r = i;
    const int ci = i, &cr = ci;
    auto a = r;
    auto b = ci;
    auto c = cr;
    auto d = &i;
    auto e = &ci;
    const auto f = ci;
    auto &g = ci;

    std::cout << "before assignments" << std::endl;
    std::cout << '\t' << "a: " << a << std::endl;
    std::cout << '\t' << "b: " << b << std::endl;
    std::cout << '\t' << "c: " << c << std::endl;

    a = 42;   // a é um int e recebe o int literal 42
    b = 42;   // b é um int e recebe o int literal 42
    c = 42;   // c é um int e recebe o int literal 42
    // d = 42;   // erro: d é um int* e não pode receber um int
    // e = 42;   // erro: e é um const int* e não pode receber um int
    // g = 42;   // erro: g é uma const int&; a variável referenciada não pode ser modificada através de g

    std::cout << "after assignments" << std::endl;
    std::cout << '\t' << "a: " << a << std::endl;
    std::cout << '\t' << "b: " << b << std::endl;
    std::cout << '\t' << "c: " << c << std::endl;

    return 0;
}

int main()
{
    int i = 3, j = 4;
    int *p = &i;

    p = &j;     // muda o valor do ponteiro p
    *p = 5;     // muda o valor da variável apontada por p
}

#include <iostream>
#include <typeinfo>

int main()
{
    const int i = 42;
    auto j = i;                 // j é int
    const auto &k = i;          // k é const int&
    auto *p = &i;               // p é const int*
    const auto j2 = i, &k2 = i; // j2 é const int; k2 é const int&

    std::cout << "j:  " << typeid(j).name()
            << "\nk:  " << typeid(k).name()
            << "\np:  " << typeid(p).name()
            << "\nj2: " << typeid(j2).name()
            << "\nk2: " << typeid(k2).name()
            << std::endl;

    return 0;
}

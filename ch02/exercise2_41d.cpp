// Baseado na solução do exercício 1.23

#include <iostream>
#include <string>

struct Sales_data
{
    std::string book_number;
    unsigned units_sold = 0;
    double revenue = 0.0;
};

int main()
{
    Sales_data curr_item, item;
    double price;

    if (std::cin >> curr_item.book_number >> curr_item.units_sold >> price) {

        int cnt = 1;

        while (std::cin >> item.book_number >> item.units_sold >> price) {
            if (item.book_number == curr_item.book_number) {
                ++cnt;
            }
            else {
                std::cout << curr_item.book_number << " occurs " << cnt << " times" << std::endl;
                curr_item = item;
                cnt = 1;
            }
        }

        std::cout << curr_item.book_number << " occurs " << cnt << " times" << std::endl;
    }

    return 0;
}

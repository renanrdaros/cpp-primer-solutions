#include <iostream>
#include "Sales_data.h"

int main()
{
    Sales_data data1, data2;
    double price;

    // recebe dados do primeiro item
    if (std::cin >> data1.book_number >> data1.units_sold >> price) {
        data1.revenue = data1.units_sold * price;
        // recebe dados do segundo item
        if (std::cin >> data2.book_number >> data2.units_sold >> price) {
            data2.revenue = data2.units_sold * price;
            // só faz a soma se os livros possuírem o mesmo ISBN
            if (data1.book_number == data2.book_number) {
                Sales_data sum;
                sum.book_number = data1.book_number;
                sum.units_sold = data1.units_sold + data2.units_sold;
                sum.revenue = data1.revenue + data2.revenue;

                std::cout << sum.book_number << ' ' << sum.units_sold << ' ' << sum.revenue << ' '
                          << ((sum.units_sold != 0) ? sum.revenue/sum.units_sold : 0.0) << std::endl;
            }
            else {
                std::cerr << "Data must refer to the same ISBN" << std::endl;
                return -1;
            }
        }
        else {
            std::cerr << "Incorrect input for item #2" << std::endl;
            return -1;
        }
    }
    else {
        std::cerr << "Incorrect input for item #1" << std::endl;
        return -1;
    }

    return 0;
}

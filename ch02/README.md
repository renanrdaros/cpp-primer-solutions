# Chapter 2. Variables and Basic Types

## Exercise 2.1

A diferença entre `short`, `int`, `long` e `long long` é a quantidade de memória que variáveis de cada um desses tipos de dados ocupam. A linguagem garante tamanhos mínimos para cada um desses tipos, além de garantir que sizeof(short) <= sizeof(int) <= sizeof(long) <= sizeof(long long).

Tipos `unsigned` só podem representar valores maiores ou iguais a zero, enquanto tipos `signed` podem representar faixas de valores que variam entre um mínimo negativo e um máximo positivo. Os mínimos e máximos dependem da quantidade de memória ocupada pela variável. Um `unsigned short` de 16 bits, por exemplo, pode conter valores inteiros na faixa [0, 65535], enquanto um `signed short` também de 16 bits pode representar valores na faixa [-32768, 32767].

`short`, `int`, `long` e `long long` são `signed` por padrão se não forem precedidos por um dos modificadores (`signed` ou `unsigned`). Se o tipo `char` não for precedido por um modificador, o padrão não define se aquele tipo será `unsigned` ou `signed`, portanto, isso irá depender do compilador.

Para tipos de ponto flutuante o padrão define um número mínimo de dígitos significativos. Tipicamente, `float`s ocupam 32 bits e possuem 7 dígitos significativos, enquanto `double`s ocupam 64 bits e possuem 16 dígitos significativos.


## Exercise 2.2

As variáveis definidas pelo problema envolvem percentuais e quantias de dinheiro, portanto, são necessários tipos de dados de ponto flutuante para representar essas variáveis. Seguindo os conselhos que os autores dão no final da seção 2.1.1, eu usaria o tipo `double`.


## Exercise 2.3

```cpp
unsigned u = 10, u2 = 42;
std::cout << u2 - u << std::endl;  // 32
std::cout << u - u2 << std::endl;  // 2^32 - 32 (assumindo que ints ocupam 32 bits)

int i = 10, i2 = 42;
std::cout << i2 - i << std::endl;  //  32
std::cout << i - i2 << std::endl;  // -32

std::cout << i - u << std::endl;   // 0
std::cout << u - i << std::endl;   // 0
```


## Exercise 2.4

[exercise2_04.cpp](exercise2_04.cpp)

```sh
$ ./exercise2_04
32
4294967264
32
-32
0
0
```


## Exercise 2.5

```cpp
'a'   // char
L'a'  // wchar_t
"a"   // const char*
L"a"  // const wchar_t*
```
```cpp
10    // int
10u   // unsigned int
10L   // long
10uL  // unsigned long
012   // int (octal)
0xC   // int (hexadecimal)
```
```cpp
3.14  // double
3.14f // float
3.14L // long double
```
```cpp
10    // int
10u   // unsigned int
10.   // double
10e-2 // double
```


## Exercise 2.6

```cpp
// inicializa month e day com os decimais 9 e 7, respectivamente
int month = 9, day = 7;

// ERRO: 9 não é um dígito válido em uma constante octal
int month = 09, day = 07;
```


## Exercise 2.7

```cpp
"Who goes with F\145rgus?\012" // "Who goes with Fergus?\n" (const char*)
```
```cpp
3.14e1L // 31.4 (long double)
```
```cpp
1024f  // ERRO: apenas literais de ponto flutuante podem usar o sufixo f
```
```cpp
3.14L  // 3.14 (long double)
```


## Exercise 2.8

[exercise2_08.cpp](exercise2_08.cpp)


## Exercise 2.9

```cpp
// Ilegal - definições de variáveis não podem ser passadas como argumentos
std::cin >> int input_value;

// Ilegal - list initialization impede conversões de estreitamento de tipo
int i = { 3.14 };

// Legal APENAS SE wage for definida antes de salary
double salary = wage = 9999.99;

// Legal, mas o valor será truncado
int i = 3.14;
```


## Exercise 2.10

```cpp
#include <string>

std::string global_str; // string vazia
int global_int;         // zero

int main()
{
    int local_int;          // indefinido
    std::string local_str;  // string vazia
}
```


## Exercise 2.11

```cpp
extern int ix = 1024;  // definição
int iy;                // definição
extern int iz;         // declaração
```


## Exercise 2.12

```cpp
int double = 3.14;      // Inválido - double é uma palavra reservada da linguagem
int _;                  // Válido
int catch-22;           // Inválido - '-' não é um caractere válido para definição de identificadores
int 1_or_2 = 1;         // Inválido - o primeiro caractere de deve ser uma letra ou underline
double Double = 3.14;   // Válido
```


## Exercise 2.13

```cpp
int i = 42;
int main()
{
    int i = 100;
    int j = i;   // j = 100
}
```


## Exercise 2.14

```cpp
int i = 100, sum = 0;
for (int i = 0; i != 10; ++i)
     sum += i;
std::cout << i << " " << sum << std::endl;
```

O fragmento de código acima é legal. O programa imprime 100 (o valor da variável `i` definida fora do escopo do laço for) seguido de 45 (o resultado do somatório implementado pelo laço for).


## Exercise 2.15

```cpp
int ival = 1.01;    // Válida
int &rval1 = 1.01;  // Inválida - o inicializador deve ser um objeto
int &rval2 = ival;  // Válida
int &rval3;         // Inválida - uma referência DEVE ser inicializada
```


## Exercise 2.16

```cpp
int i = 0, &r1 = i;
double d = 0, &r2 = d;

r2 = 3.14159;   // Válida - d = 3.14159
r2 = r1;        // Válida - d = i
i = r2;         // Válida - i = d
r1 = d;         // Válida - i = d
```


## Exercise 2.17

```cpp
int i, &ri = i;
i = 5; ri = 10;
std::cout << i << " " << ri << std::endl;
```

O programa imprime `10 10`.


## Exercise 2.18

[exercise2_18.cpp](exercise2_18.cpp)


## Exercise 2.19

Uma referência é um apelido para um objeto; é um identificador que, durante a inicialização, é ligado a um objeto e passa a referenciá-lo durante toda a sua existência. Referências não ocupam memória, devem ser inicializadas e não podem ser alteradas; uma vez inicializadas, referenciam sempre o mesmo objeto. Os objetos referenciados são acessados através da referência exatamente da mesma forma que se acessa qualquer outra variável; não é necessário utilizar nem um operador ou sintaxe especial.

Um ponteiro é um objeto; ele ocupa memória e (salvo uma exceção que ainda será vista) pode ter seu valor alterado, ou seja, pode apontar para várias diferentes posições de memória durante sua existência. Os objetos apontados pelo ponteiro são acessados através dele com o operador de indireção (dereference operator).


## Exercise 2.20

```cpp
int i = 42;
int *p1 = &i;
*p1 = *p1 * *p1;
```

Calcula o quadrado do valor armazenado em `i` e armazena o resultado nessa mesma variável.


## Exercise 2.21

```cpp
int i = 0;
double* dp = &i;    // Ilegal - o tipo da variável é diferente do tipo de ponteiro
int *ip = i;        // Ilegal - ponteiros armazenam endereços de memória, não números inteiros
int *p = &i;        // Legal  - p recebe o endereço de i e passa a apontar para essa variável
```


## Exercise 2.22

```cpp
// testa o conteúdo do ponteiro p
// falso apenas se p == 0 ou p == nullptr, verdadeiro em qualquer outro caso
if (p)

// testa o conteúdo da variável apontada por p
// falso se a variável apontada por p for zero, verdadeiro em qualquer outro caso
if (*p) 
```

## Exercise 2.23

Não. // TODO: Why not?


## Exercise 2.24

```cpp
int i = 42;    

// essa inicialização é legal porque void* é um tipo especial de ponteiro
// que pode apontar para variáveis de quaisquer tipos de dados
void *p = &i;  

// essa inicialização é ilegal porque o tipo de i é diferente do tipo
// do ponteiro lp
long *lp = &i;
```


## Exercise 2.25

```cpp
// considerando que as definições a seguir não são globais:

// ip é um ponteiro de int com valor indefinido
// i é um int com valor indefinido
// r é uma referência de int ligada à variável i
int* ip, i, &r = i;

// i é um int com valor indefinido
// ip é um ponteiro inicializado com zero (ponteiro nulo)
int i, *ip = 0;

// ip é um ponteiro de int com valor indefinido
// ip2 é um int com valor indefinido
int* ip, ip2;
```


## Exercise 2.26

```cpp
const int buf;      // Ilegal - constantes devem ser inicializadas
int cnt = 0;        // Legal
const int sz = cnt; // Legal
++cnt;              // Legal
++sz;               // Ilegal - sz é uma constante; seu valor não pode ser modificado
```


## Exercise 2.27

```cpp
int i = -1, &r = 0;         // Ilegal - r (reference to nonconst) não pode ser ligada a um literal
```
```cpp
int i2;
int *const p2 = &i2;        // Legal
```
```cpp
const int i = -1, &r = 0;   // Legal
```
```cpp
int i2;
const int *const p3 = &i2;  // Legal
```
```cpp
int i2;
const int *p1 = &i2;        // Legal
```
```cpp
const int &const r2;        // Ilegal - 1) referências devem ser inicializadas
                            //          2) não faz sentido qualificar uma referência como const
```
```cpp
int i;
const int i2 = i, &r = i;   // Legal
```


## Exercise 2.28

```cpp
int i, *const cp;       // Ilegal - cp é um ponteiro constante e, portanto, deve ser inicializado
int *p1, *const p2;     // Ilegal - p2 é um ponteiro constante e, portanto, deve ser inicializado
const int ic, &r = ic;  // Ilegal - ic é um inteiro constante e, portanto, deve ser inicializado
const int *const p3;    // Ilegal - p3 é um ponteiro constante e, portanto, deve ser inicializado
const int *p;           // Legal
```


## Exercise 2.29

```cpp
i = ic;     // Legal - um int pode receber o valor de um const int
p1 = p3;    // Ilegal - um int* não pode receber o valor de um const int*
p1 = &ic;   // Ilegal - um int* não pode apontar para um const int
p3 = &ic;   // Ilegal - p3 é um ponteiro constante e, portanto, não pode ter seu valor modificado
p2 = p1;    // Ilegal - p2 é um ponteiro constante e, portanto, não pode ter seu valor modificado
ic = *p3;   // Ilegal - ic é uma constante e, portanto, não pode ter seu valor modificado
```


## Exercise 2.30

```cpp
const int v2 = 0;            // v2 - top-level const
int v1 = v2;                 
int *p1 = &v1, &r1 = v1;     
const int *p2 = &v2,         // p2 - low-level const
          *const p3 = &i,    // p3 - low-level e top-level const
          &r2 = v2;          // r2 - low-level const
```


## Exercise 2.31

```cpp
r1 = v2;    // Legal
p1 = p2;    // Ilegal - um ptr low-level const só pode ser atribuído a outro ptr low-level const
p2 = p1;    // Legal
p1 = p3;    // Ilegal - um ptr low-level const só pode ser atribuído a outro ptr low-level const
p2 = p3;    // Legal
```


## Exercise 2.32

```cpp
int null = 0, *p = null;    // Ilegal
```
```cpp
// correção
constexpr int null = 0;
int *p = +null;           
```

**Nota**: A resolução desse exercício envolve conceitos ainda não apresentados no livro. Se quiser saber mais, dê uma olhada [nesta questão](https://stackoverflow.com/questions/24213159/how-to-initialize-a-pointer-using-an-object-with-0-value) no Stack Overflow.


## Exercise 2.33

```cpp
int i = 0, &r = i;
auto a = r;
const int ci = i, &cr = ci;
auto b = ci;
auto c = cr;
auto d = &i;
auto e = &ci;
const auto f = ci;
auto &g = ci;

a = 42;   // a é um int e recebe o int literal 42
b = 42;   // b é um int e recebe o int literal 42
c = 42;   // c é um int e recebe o int literal 42
d = 42;   // erro: d é um int* e não pode receber um int
e = 42;   // erro: e é um const int* e não pode receber um int
g = 42;   // erro: g é uma const int&; a variável referenciada não pode ser modificada através de g
```


## Exercise 2.34

[exercise2_34.cpp](exercise2_34.cpp)

```sh
$ ./exercise2_34
before assignments
	a: 0
	b: 0
	c: 0
after assignments
	a: 42
	b: 42
	c: 42
```


## Exercise 2.35

```cpp
const int i = 42;
auto j = i;                 // j é int
const auto &k = i;          // k é const int&
auto *p = &i;               // p é const int*
const auto j2 = i, &k2 = i; // j2 é const int; k2 é const int&
```

[exercise2_35.cpp](exercise2_35.cpp)


## Exercise 2.36

```cpp
int a = 3, b = 4;
decltype(a) c = a;      // c é int
decltype((b)) d = a;    // d é int&
++c;                    
++d;
```
Ao fim da execução do fragmento de código acima:
* `a == 4`
* `b == 4`
* `c == 4`
* `d == 4`


## Exercise 2.37

```cpp
int a = 3, b = 4;
decltype(a) c = a;      // c é int
decltype(a = b) d = a;  // d é int&
```
Ao fim da execução do fragmento de código acima:
* `a == 3`
* `b == 4`
* `c == 3`
* `d == 3`


## Exercise 2.38

Variáveis definidas com `decltype()` possuem exatamente o mesmo tipo do argumento passado para `decltype()`, incluindo referências e top-level const. Variáveis definidas com o especificador `auto` nem sempre possuem exatamente o mesmo tipo do inicializador.

Exemplos:

```cpp
int i = 3;
const int ci = 5;

auto a = i;          // a é int (possui o mesmo tipo do inicializador)
decltype(i) b;       // b é int (possui o mesmo tipo do argumento de decltype)

auto c = ci;         // c é int (NÃO possui o mesmo tipo do inicializador)
decltype(ci) d = 3;  // d é const int (possui o mesmo tipo do argumento de decltype)
```


## Exercise 2.39

[exercise2_39.cpp](exercise2_39.cpp)

```sh
$ g++ exercise2_39.cpp -o exercise2_39
exercise2_39.cpp:1:28: error: expected ‘;’ after struct definition
 struct Foo { /* empty   */ } // Note: no semicolon
                            ^
```


## Exercise 2.40

[exercise2_40.h](exercise2_40.h)


## Exercise 2.41

[exercise2_41a.cpp](exercise2_41a.cpp)

[exercise2_41b.cpp](exercise2_41b.cpp)

[exercise2_41c.cpp](exercise2_41c.cpp)

[exercise2_41d.cpp](exercise2_41d.cpp)

```sh
# exercise2_41e

$ g++ exercise2_41d.cpp -o exercise2_41d
$ ./exercise2_41d < data/book_sales
0-201-70353-X occurs 1 times
0-201-82470-1 occurs 1 times
0-201-88954-4 occurs 4 times
0-399-82477-1 occurs 2 times
0-201-78345-X occurs 2 times
```

[exercise2_41f.cpp](exercise2_41f.cpp)

```sh
# exercise2_41f

$ g++ exercise2_41f.cpp -o exercise2_41f
$ ./exercise2_41f < data/book_sales
0-201-70353-X 4 99.96 24.99
0-201-82470-1 4 181.56 45.39
0-201-88954-4 16 198 12.375
0-399-82477-1 5 226.95 45.39
0-201-78345-X 5 110 22
```


## Exercise 2.42

[Sales_data.h](Sales_data.h)

[exercise2_42a.cpp](exercise2_42a.cpp)

[exercise2_42b.cpp](exercise2_42b.cpp)

[exercise2_42c.cpp](exercise2_42c.cpp)

[exercise2_42d.cpp](exercise2_42d.cpp)

```sh
# exercise2_42e

$ g++ exercise2_42d.cpp -o exercise2_42d
$ ./exercise2_42d < data/book_sales
0-201-70353-X occurs 1 times
0-201-82470-1 occurs 1 times
0-201-88954-4 occurs 4 times
0-399-82477-1 occurs 2 times
0-201-78345-X occurs 2 times
```

[exercise2_42f.cpp](exercise2_42f.cpp)

```sh
# exercise2_42f

$ g++ exercise2_42f.cpp -o exercise2_42f
$ ./exercise2_42f < data/book_sales
0-201-70353-X 4 99.96 24.99
0-201-82470-1 4 181.56 45.39
0-201-88954-4 16 198 12.375
0-399-82477-1 5 226.95 45.39
0-201-78345-X 5 110 22
```
